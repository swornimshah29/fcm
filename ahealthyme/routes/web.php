<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



/*
Crud operation on program schedules
*/

Route::post('api/auth/user/schedule_store',"ProgramScheduleController@save");
Route::post('api/auth/user/schedule_delete',"ProgramScheduleController@delete");
Route::post('api/auth/user/schedule_update',"ProgramScheduleController@update");
Route::post('api/auth/user/send',"ProgramScheduleController@sendNotification");

