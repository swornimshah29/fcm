<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

error_reporting(E_ALL);
// ini_set('display_errors', 1);
class ProgramScheduleController extends Controller
{
    public function sendNotification(Request $request){
        // echo "path to the file is ".__DIR__;

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/service_account.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->create();



        $response=$firebase
        ->getMessaging()
        ->send([
            //'topic' => 'my-topic',
            // 'condition' => "'TopicA' in topics && ('TopicB' in topics || 'TopicC' in topics)",
            //'token' => 'ekhaKQWS3ZA:APA91bGImVbV1uOjT4Lscyn-2K5VaC2Fia4HLuj1zRrFuQsAGHp6AK9agvycsAWXIBWN5zIV6VqQ5TTf2PiB164u7UdpTOsl3vlAT0r2CFyak7fx1uPpUoji2evnjtQiFnNfUUs_FrPR',//ios
            'token' => 'dap10-P6A1Y:APA91bEnBM4JWnVPZiRIkxLYnAJDA0-EBT0hroVLjjEvlxiN3-qXEKNHwodGwt1KeWqjiYwYLKNA13n-NyQzlKboOY956XO2NfT8Rkde_fYa-tNgMWWppKY3XP3yWxtLFqlISwn6CVgo',//android

            'notification' => [
                'title' => 'POSTMAN TITLE',
                'body' => 'POSTMAN BODY',
                'sound'=>'default',
                'color' => 'blue'
            ],
            // 'data' => [
            //     'key_1' => 'Value 1',
            //     'key_2' => 'Value 2',
            // ],
            'android' => [
                'ttl' => '3600s',
                'priority' => 'high',
                'notification' => [
                    'title' => 'POSTMAN TITLE',
                    'body' => 'POSTMAN BODY',
                    'icon' => 'stock_ticker_update',
                    'sound'=>'default',
                    'color' => 'blue'
                    ],
            ],
            'apns' => [
                'headers' => [
                    'apns-priority' => '10',
                ],
                'payload' => [
                    'aps' => [
                        'alert' => [
                            'title' => 'POSTMAN TITLE',
                            'body' => 'POSTMAN BODY',
                            'sound'=>'default',
                            'color' => 'blue'
                        ],
                        'badge' => 42,
                    ],
                ],
            ],
            'webpush' => [
                'notification' => [
                    'title' => '$GOOG up 1.43% on the day',
                    'body' => '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
                    'icon' => 'https://my-server/icon.png',
                ],
                'fcm_options' => [
                    'link' => 'https://my-server/some-page',
                ],
            ],
        ]);

        echo json_encode($response);


    }

}
